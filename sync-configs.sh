#!/bin/sh

set -eu

# Symlink theme files for current user
mkdir -p "$HOME/.config/gtk-2.0"
mkdir -p "$HOME/.config/gtk-3.0"
ln -srf "$HOME/.config/gtk-themes/.gtkrc-2.0"   "$HOME/.config/gtk-2.0/gtkrc"
ln -srf "$HOME/.config/gtk-themes/settings.ini" "$HOME/.config/gtk-3.0/settings.ini"
touch "$HOME/.config/gtk-3.0/bookmarks"

# Copy theme files for root
sudo mkdir -p /root/.config/gtk-3.0
sudo cp "$HOME/.config/gtk-themes/.gtkrc-2.0" /root/.gtkrc-2.0
sudo cp "$HOME/.config/gtk-themes/settings.ini" /root/.config/gtk-3.0/settings.ini

# Symlink Adwaita theme to Adwaita-dark for this user
# overriding the light theme with the dark theme
mkdir -p "$HOME/.local/share/themes"
[ ! -e "$HOME/.local/share/themes/Adwaita" ] && \
	ln -sr /usr/share/themes/Adwaita-dark "$HOME/.local/share/themes/Adwaita"

# Symlink Adwaita theme to Adwaita-dark for root
sudo mkdir -p /root/.themes/
sudo ln -srf /usr/share/themes/Adwaita-dark /root/.themes/Adwaita

#mkdir -p "$HOME/.local/share/gtksourceview-2.0/styles/"
#ln -srf "$HOME/.config/gtk/Yaru-dark.xml" "$HOME/.local/share/gtksourceview-2.0/styles/Yaru-dark.xml"

# Installing these seemingly unused packages removes some warnings from console
# sudo apt-get install -y gtk2-engines-pixbuf gtk2-engines-murrine gnome-themes-extra
